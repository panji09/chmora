function Startup(){
// input form
$('#InputForm').dialog({
		bgiframe: true,
		autoOpen: false,
		height: 'auto',
		width: 705,
		modal: true,
		close: function() {
			//allFields.val('').removeClass('ui-state-error');
		}
	}, 
		ResetModul
	);

	$('#btn_hapus').click(function(){DeleteData();});
	
	$('#propinsi').change( function(){ LoadDataKota(); });
	
	$('#kota').change( function(){ LoadDataKecamatan(); });

	$('#kecamatan').change(function(){ LoadDataDesa(); });

	$('#filter').click(function(){
		
		LoadDataPengaduan();
	});
	
// click simpan data
	$('#simpan').click(function(){ SimpanData();});
}

// 


// delete data
function DeleteData(){
	jConfirm('Anda Yakin Menghapus Data Ini ?', 'Hapus Data', function(r) {
		
		if(r){ // ya, hapus data
			var kd_pengaduan = $('#kd_pengaduan').val();
			$.post("./g3n.php?pengaduan/Delete", 
				{
					'KdPengaduan': kd_pengaduan
				}, function(data){
					//
					var status = "BERHASIL";
					if(!data.status){
						status = "GAGAL, SILAHKAN ULANGI";					
					}
					else{
						$('#InputForm').dialog('close');
						LoadDataPengaduan();
					}
					jAlert(status, "Hapus Data");
					
				}, "json"
			);
		}
	});

}

// simpan data
function SimpanData(){
	
	var 
	id	= $('#id').val(), // jika edit data
	
	kd_pengaduan = $('#kd_pengaduan').val(), // if edit mode
	
	propinsi = $('#propinsi').val(),
	kota	= $('#kota').val(),
	kecamatan = $('#kecamatan').val(),
	desa	= $('#desa').val(),
	jenjang	= $('#jenjang').val(),
	nama_sekolah = $('#nama_sekolah').val(),
	
	sumber_informasi =   $('input[name=sumber_informasi]:checked').val(),
	sumber_informasi_lain = $('#sumber_informasi_lain').val(),
	
	pKomite		= $('#pKomite').val(),
	pGuru 		= $('#pGuru').val(),
	pBendSek	= $('#pBendSek').val(),
	pLsm 		= $('#pLsm').val(),
	pDinas 		= $('#pDinas').val(),
	pUPTD 		= $('#pUPTD').val(),
	pLain 		= $('#pLain').val(),
	pelaku_lain	= $('#pelaku_lain').val(),
	
	kategori_pengaduan	= $('input[name=kategori_pengaduan]:checked').val(),
	jumlah_dana = $('#jumlah_dana').val(),
	
	tgl_kejadian	= $('#tgl_kejadian').val(),
	deskripsi_pengaduan	= $('#deskripsi_pengaduan').val(),
	
	penanganan	= $('input[name=tangani]:checked').val(),//$('#penanganan').val(),
	slc_penanganan	= $('#slc_penanganan').val(),
	
	status =  $('input[name=prosel]:checked').val();//$('#prosel').val();
	
	// send data
	$.post("./g3n.php?ponline/Simpan", 
		{
			'KdPengaduan': kd_pengaduan,
			'KdProv'	: propinsi,
			'KdKab'		: kota,
			'KdKec'		: kecamatan,
			'KdDesa'	: desa,
			'KdJenjang'	: jenjang,
			'NmSekolah'	: nama_sekolah,
			
			'KdSumber'	: sumber_informasi,
			'LainSumber'	: sumber_informasi_lain,
			
			'pKomite'	: pKomite,
			'pGuru'	: pGuru,
			'pBendSek'	: pBendSek,
			'pLsm'	: pLsm,
			'pDinas': pDinas,
			'pUPTD'	: pUPTD,
			'pLain'	: pLain,
			'KetpLain'	: pelaku_lain,
			
			'KdKategori'	: kategori_pengaduan,
			'KetRpKat4'	: jumlah_dana,
			
			'TglKetahui': tgl_kejadian,
			'Deskripsi'	: deskripsi_pengaduan,
			
			'KdHukum'	: penanganan,
			'KdOleh'	: slc_penanganan,
			'KdProSel'	: status		
			 
		},
		function(data){
			if(!kd_pengaduan){ // input
				ResetModul();
			} else {
				$('#InputForm').dialog('close');
			}
			jAlert(data.status, 'Simpan Data');
			LoadDataPengaduan();
			
		}, "json"
	);
}

// get one data and view it to input box
function GetBy(kd_pengaduan){
	//jAlert(kd_pengaduan,'Kode Pengaduan');
	
	$('#dialog').dialog('open');
	
	$.post("./g3n.php?ponline/GetBy", 
		{
			'KdPengaduan': kd_pengaduan
		}, function(data){
			
			ResetModul();
			$('#InputForm').dialog('open');
			for ( var i in data ) {
				// kode pengaduan
				$('#kd_pengaduan').val(data[i].KdPengaduan);
				// load daerah
				LoadDataPropinsi(data[i].propinsi,data[i].kota,data[i].kecamatan,data[i].desa);
				// jenjang sekolah
				LoadJenjang(data[i].jenjang);
				// nama sekolah
				$('#nama_sekolah').val(data[i].nama);
				// set kategori
				LoadDivKategoriPengaduan(data[i].kategori);
				if(data[i].kategori == 4){
					$('#jumlah_dana').removeAttr('disabled');
					$('#jumlah_dana').val(data[i].rp);
				}
				//sumber info
				LoadDivSumberInformasi(data[i].info);
				if(data[i].info == 8){
					$('#sumber_informasi_lain').removeAttr('disabled');
					$('#sumber_informasi_lain').val(data[i].info_lain);
				}
				// tgl pengaduan
				$('#tgl_kejadian').val(data[i].tgl);
				// deskripsi
				$('#deskripsi_pengaduan').val(data[i].deskripsi);
				// pelaku
				var val = new Array(data[i].pKomite, data[i].pGuru, data[i].pBendSek, data[i].pLSM,data[i].pDinas, data[i].pUPTD, data[i].pLain);
				LoadDivPelaku(val);
				// set lainny
				if(data[i].pLain == 'TRUE'){
					$('#pelaku_lain').removeAttr('disabled');
					$('#pelaku_lain').val(data[i].pelaku_lain);
				}
				
				//jAlert('Oleh @GetBy: '+data[i].oleh);
			
				
				// hukum non
				LoadHukumNonHukum(data[i].hukum,'#div_tangani',data[i].oleh,'#slc_penanganan');
				// oleh
				cmb_penanganan(data[i].hukum,data[i].oleh, '#slc_penanganan');
				// prosel
				LoadProSel(data[i].status, '#div_prosel');
				// munculkan tombol delete
				$('#btn_hapus').show();
				
				// tampilkan kode pengeduan
				$('#span_kdpengaduan').html(data[i].KdPengaduan);
				
				// hide dialog
				$('#dialog').dialog('close');
			}
			
			
		}, "json"
	);
}

// reset select jenjang
function LoadJenjang(pilih){
	var ark = new Array("SD","SMP","SD dan SMP","Lainnya");
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str +=  "<option value='"+i+"' ";
		if(i == pilih)
			str += "selected";
		str += "> "+ark[i-1]+"</option>";
	}
	$('#jenjang').html(str);
}

// reset radio div_kategori_pengaduan
function LoadDivKategoriPengaduan(pilih){
	
	//jAlert(pilih,'Pilih');
	var ark = new Array("Pertanyaan", "Saran", "Penyimpangan Peraturan", "Ketidaksesuaian<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penggunaan Dana");
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str += "<input type='radio' name='kategori_pengaduan' id='kategori_pengaduan' value='"+i+"' ";
		if(i < l){
			str += "onclick=\"disable('#jumlah_dana');\" ";
		} else {
			str += "onclick=\"view(this,'#jumlah_dana');\" ";
		}
		if(i != pilih)
			str += "/> "+ark[i-1]+"<br />";
		else 
			str += "checked=\"checked\"/> "+ark[i-1]+"<br />";
	}
	$('#div_kategori_pengaduan').html(str);
}

// reset div sumber informasi
function LoadDivSumberInformasi(pilih){
	var ark = new Array("Orangtua Siswa", "Komite Sekolah", "Guru \/ Pegawai Sekolah", "Kepala Sekolah", "Tim Manajement BOS", "Media Massa", "Hasil Audit", "Lainnya");
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str += "<input type='radio' name='sumber_informasi' id='sumber_informasi' value='"+i+"' ";
		if(i < l){
			str += "onclick=\"disable('#sumber_informasi_lain');\" ";
		} else {
			str += "onclick=\"view(this,'#sumber_informasi_lain');\" ";
		}
		if(i != pilih)
			str += "/> "+ark[i-1]+"<br />";
		else 
			str += "checked='checked'/> "+ark[i-1]+"<br />"; 
	}
	
	$('#div_sumber_informasi').html(str);

}

// reset status penanganan
function LoadHukumNonHukum(pilih, div, oleh,target){
	var ark = new Array("Non Hukum", "Hukum");
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str += "<td><input type='radio' name='tangani' id='tangani' value='"+i+"' ";
		str += "onclick=\"cmb_penanganan("+i+",'"+oleh+"','"+target+"');\""
		if(i == pilih)
			str += "checked='checked'";
		str += "/> "+ark[i-1]+"</td>";
	}
	//$('#div_tangani').html(str);
	$(div).html(str);
}

// reset status penanganan
function LoadProSel(pilih, div){
	var ark = new Array("Proses", "Selesai");
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str += "<td><input type='radio' name='prosel' id='prosel' value='"+i+"' ";
		if(i == pilih)
			str += "checked='checked'";
		str += "/> "+ark[i-1]+"</td>";
	}
	//$('#div_prosel').html(str);
	$(div).html(str);
}

// reset status penanganan
function LoadDivPelaku(value){
	if(value.length == 0) var value = new Array('FALSE','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE');
	var ark = new Array('pKomite', 'pGuru','pBendSek','pLsm','pDinas','pUPTD', 'pLain');
	var brk = new Array('Komite Sekolah', 'Guru/Pegawai Sekolah','Kepala/Bendahara Sekolah','LSM','Dinas Pendidikan','UPTD/Pengawas', 'Lain-lain');
	var l = ark.length;
	var str = '';
	for (var i = 1; i <= l ; i++) {
		str += "<input type='checkbox' name='"+ark[i-1]+"' id='"+ark[i-1]+"' ";
		
		if(i < l){
			str += " onchange=\"rdbchange(this)\" ";
		} else {
			str +=  "onclick=\"view(this,'#pelaku_lain');rdbchange(this);\" ";
		}
		
		str += "value='"+value[i-1]+"' ";
		if(value[i-1]=='TRUE') str += " checked='checked' ";
		str += "/> "+brk[i-1]+"<br />";
	}
	$('#div_pelaku').html(str);
}

//mengambil data dari database dengan method ajax 
function LoadDataPengaduan(){
	
	var tahun = $('#filter-tahun').val();
	var triwulan = $('#filter-triwulan').val();
	var jenjang = $('#filter-jenjang').val();
	//alert(tahun+' '+triwulan);
	
	//if(triwulan == 0) triwulan = 'all';
	
	$('#dialog').dialog('open');
	
	//alert(triwulan);
	$.post("./g3n.php?ponline/LoadPengaduan", 
		{'tahun':tahun, 'triwulan':triwulan, 'jenjang':jenjang},
		function(data){ 
			var strbody = '';
			for(var i in data){
				var nomor = i; nomor++;
				if(data[i].hari > 180 && data[i].updates == 0) strbody += "<tr class='alert'>";
     			else strbody += "<tr>";
					strbody += "<td>"+ nomor +"</td>";
					strbody += "<td>"+ data[i].tgl +"</td>";
					strbody += "<td>"+ data[i].sekolah +"</td>";
					strbody += "<td>"+ data[i].deskripsi +"</td>";
					strbody += "<td>"+ data[i].propinsi+" "+data[i].kota +" "+data[i].kecamatan + " " +data[i].desa +"</td>";
					strbody += "<td>"+ data[i].KdPengaduan +"</td>";
					strbody += "<td>"+ "<input type='button' class='btn' value='Detail' onclick=\"GetBy('"+data[i].KdPengaduan+"');\"/> <input type='button' class='btn' value='Update' onclick=\"FormUpdate('"+data[i].KdPengaduan+"');\"/>" +"</td>";
     			strbody += "</tr>";
			}
			$('#table-data-body').html(strbody);
			$('#dialog').dialog('close');
	
		}, 
		"json"
	);
}

// load data build for what ?
function LoadData(){
	// load data pengaduan
	LoadDataPengaduan();
}

// change every input thing to zero
function ResetModul(){
	$('#btn_hapus').hide();
	
	$('#id').val(0); // jika edit data
	
	$('#kd_pengaduan').val(''); // if edit mode
	
	LoadDataPropinsi('','','',''); // RESET PROPINSI, KAB, KEC, DESA
	LoadJenjang();
	LoadDivSumberInformasi();
	disable('#sumber_informasi_lain');

	var value = new Array('FALSE','FALSE','FALSE','FALSE','FALSE');
	LoadDivPelaku(value);
	disable('#pelaku_lain');
	
	$('#nama_sekolah').val(''); 
	
	LoadDivKategoriPengaduan();
	disable('#jumlah_dana');
	
	$('#tgl_kejadian').val('');
	$('#deskripsi_pengaduan').val('');
	
	//LoadHukumNonHukum();
	LoadHukumNonHukum('','#div_tangani',0,'#slc_penanganan');
	
	$('#slc_penanganan').val('');
	LoadProSel(value,'#div_prosel');
	// hilangkan button hapus
	
	
}

// load the combox propince
function LoadDataPropinsi(p,k,kc,d){
	$.post("./g3n.php?pengaduan/LoadPropinsi", 
		{},
		function(data){ 
			var strbody = '';
			for(var i in data){
				var nomor = i; nomor++;
				strbody += "<option value='"+data[i].kode+"' ";
				if(data[i].kode == p) strbody += "selected";
				strbody += ">"+data[i].propinsi+"</option>";
			}
			$('#propinsi').html(strbody);
			LoadDataKota(k,kc,d);
		}, 
		"json"
	);
	
}

// load the combox Kota
function LoadDataKota(k,kc,d){
	var propinsi = $('#propinsi').val();
	$.post("./g3n.php?pengaduan/LoadKabupaten", 
		{'propinsi':propinsi},
		function(data){ 
			var strbody = '';
				for(var i in data){
					var nomor = i; nomor++;
     				strbody += "<option value='"+data[i].kode+"' ";
     				if(data[i].kode == k) strbody += "selected";
     				strbody += ">"+data[i].kota+"</option>";
				}
				$('#kota').html(strbody);
				LoadDataKecamatan(kc,d);
		}, 
		"json"
	);
}

// load the combox Kecamatan
function LoadDataKecamatan(kc,d){
	var kabupaten = $('#kota').val();
	$.post("./g3n.php?pengaduan/LoadKecamatan", 
		{'kabupaten':kabupaten},
		function(data){ 
			var strbody = '';
				for(var i in data){
					var nomor = i; nomor++;
     				strbody += "<option value='"+data[i].kode+"' ";
     				if(data[i].kode == kc) strbody += "selected";
     				strbody += ">"+data[i].kecamatan+"</option>";
				}
				$('#kecamatan').html(strbody);
				LoadDataDesa(d);
		}, 
		"json"
	);
}

// load the combox Desa
function LoadDataDesa(d){
	var kecamatan = $('#kecamatan').val();
	$.post("./g3n.php?pengaduan/LoadDesa", 
		{'kecamatan':kecamatan},
		function(data){ 
			var strbody = '';
			strbody += "<option value='0'>Pilih Desa</option>";
				for(var i in data){
					var nomor = i; nomor++;
     				strbody += "<option value='"+data[i].kode+"' ";
     				if(data[i].kode == d) strbody += "selected";
     				strbody += ">"+data[i].desa+"</option>";
				}
			$('#desa').html(strbody);
		}, 
		"json"
	);
}

function Rekam(){
	$('#InputForm').attr('title','Input Pengaduan');
	LoadDataPropinsi();
}

function cmb_penanganan(id,oleh,slc){ //g3n1k
	if(id == 1) var data = new Array('Sekolah', 'BOS Kab/Kota', 'BOS Prov', 'BOS Pusat');
	else var data = new Array('Kepolisian', 'Kejaksaan', 'Pengadilan', 'Putusan');
	
	var strbody = ''; var i = 0; var max = data.length;
	//jAlert('max = '+max);
	for(i=0; i<max; i++){
		//var nomor = parseInt(i)+1;
		if(i != oleh)
			strbody += "<option value='"+i+"'>"+data[i]+"</option>";
		else
			strbody += "<option value='"+i+"' selected>"+data[i]+"</option>"; 
		
	}

	$(slc).html(strbody);
}

function rdbchange(cb){
	if(cb.checked == 1){
		$(cb).val('TRUE');
	} else {
		$(cb).val('FALSE');
	}
}

function view(cb, id){
	
	if(cb.checked == 1){
		$(id).removeAttr('disabled');
	} else {
		$(id).attr('disabled','disabled');
		$(id).val('');
	}
}

function disable(id){
	$(id).attr('disabled','disabled');
	$(id).val('');
}
// ------------------------------------------------------- update -------------------
function SimpanUpdate(){
	//alert('simpan update');
	var 
	id	= $('#id_update').val(), // jika edit data
	KdPengaduan	= $('#KdPengaduanUpdate').val(), // jika edit data
	tgl_progress = $('#tgl_progress').val(),
	uraian_update = $('#uraian_update').val(),
	jumlah = $('#jumlah').val();
	
	penanganan	= $('input[name=tangani]:checked').val(),//$('#penanganan').val(),
	slc_penanganan	= $('#slc_penanganan_update').val(),
	
	status =  $('input[name=prosel]:checked').val();//$('#prosel').val();
	
	//jAlert(penanganan+' - '+slc_penanganan+' - '+status);
	// send data
	
	$.post("./g3n.php?pengaduan/Update", 
		{
			'id': id,
			'KdPengaduan': KdPengaduan,
			'TglProgress'	: tgl_progress,
			'Pengembalian'	: jumlah,
			'Uraian'	: uraian_update,
			
			'KdHukum'	: penanganan,
			'KdOleh'	: slc_penanganan,
			'KdProSel'	: status
		},
		function(data){
			if(data.status){
				if(id == 0 || id =='0'){
					jAlert('Berhasil Input Data Update', 'Input/Edit Data Update');
				} else {
					jAlert('Berhasil Edit Data Update', 'Input/Edit Data Update');
				}
				LoadDataUpdate($('#KdPengaduanUpdate').val());
				
				$('#form_update').hide('slow');
				$('#param_tbl_update').val('hide');	
				$('#show_field_update').show('slow');
			} else {
				jAlert('Gagal Menghapus Data Update', 'Hapus Data Update');
			}
		}, "json"
	);
	/* */
}

// change every update things to zero
function ResetUpdate(){
	$('#btn_update_hapus').hide();
	$('#tgl_progress').val('');
	$('#jumlah').val('');
	$('#uraian_update').val('');
	$('#id_update').val('');
	// reset progress & tangani
	$('#field_progress').show();
	// isi bagian progress
	
	//var id = 
	
	var kd_pengaduan = $('#KdPengaduanUpdate').val();
	
	$.post("./g3n.php?pengaduan/GetBy", 
	{
		'KdPengaduan': kd_pengaduan
		}, function(data){
		for ( var i in data ) {
			LoadHukumNonHukum(data[i].hukum, '#div_tangani_update', data[i].oleh,'#slc_penanganan_update');
			cmb_penanganan(data[i].hukum,data[i].oleh, '#slc_penanganan_update');
			LoadProSel(data[i].status, '#div_prosel_update');
		}
		}, "json"
	);
	
}

function LoadDataUpdate(kd_pengaduan){
	$('#dialog').dialog('open');
	//alert('LoadDataUpdate');
	$.post("./g3n.php?pengaduan/LoadUpdate", 
		{'KdPengaduan':kd_pengaduan},
		function(data){ 
			var strbody = '';
			for(var i in data){
				var nomor = i; nomor++;
				strbody += "<tr>";
					strbody += "<td>"+ nomor +"</td>";
					strbody += "<td>"+ data[i].tgl +"</td>";
					strbody += "<td>"+ data[i].uraian +"</td>";
					strbody += "<td>"+ data[i].rp +"</td>";
					//strbody += "<td>"+ "<input type='button' class='btn' value='Edit' onclick=\"GetUpdate('"+data[i].id+"');\"/>" +"</td>";
				strbody += "</tr>";
			}
			$('#table-update-body').html(strbody);
			$('#dialog').dialog('close');
		},  //data.jumlah.replace(/[\,]/g,".")
		"json"
	);
}

// get list data to update box
function FormUpdate(kd_pengaduan){
	$('#UpdateForm').dialog('open');
	$('#KdPengaduanUpdate').val(kd_pengaduan);
	//$('#InputForm').attr('title', 'Update Pengaduan');
	
	$('#field_update').hide();
	$('#field_progress').hide();
	
	$('#param_tbl_update').val('hide');	
	$('#show_field_update').show();
	
	$('#btn_update_hapus').hide();
	
	// progress
	$('#form_update').hide();

	
	ResetUpdate();
	LoadDataUpdate(kd_pengaduan);
}

function GetUpdate(id){
	$('#dialog').dialog('open');
	$.post("./g3n.php?pengaduan/GetUpdate", 
		{'id':id},
		function(data){ 
			var strbody = '';
			for(var i in data){
				$('#id_update').val(data[i].id);
				$('#KdPengaduanUpdate').val(data[i].KdPengaduan);
				
				$('#tgl_progress').val(data[i].tgl);
				$('#jumlah').val(data[i].rp);
				$('#uraian_update').val(data[i].uraian);
			}
			
			$('#field_update').show('slow');
			$('#param_tbl_update').val('show');	
			$('#show_field_update').hide('slow');
			
			$('#btn_update_hapus').show('slow');
			$('#dialog').dialog('close');
			
			// show field_progress
			$('#field_progress').show('slow');
		}, 
		"json"
	);
}

// delete update
function HapusUpdate(){
	var id = $('#id_update').val();
	$.post("./g3n.php?pengaduan/DeleteUpdate", 
		{'id':id},
		function(data){ 
			if(data.status){
				jAlert('Berhasil Menghapus Data Update', 'Hapus Data Update');
				LoadDataUpdate($('#KdPengaduanUpdate').val());
				$('#field_update').hide('slow');
				$('#param_tbl_update').val('hide');	
				$('#show_field_update').show('slow');
			} else {
				jAlert('Gagal Menghapus Data Update', 'Hapus Data Update');
			}
		}, 
		"json"
	);
}
