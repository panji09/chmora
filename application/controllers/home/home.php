<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'select_db', 'admin_handling/respon_db', 'handling/report_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		session_start();
		
	}
	
	public function index(){
		$data['title']='Pelayanan dan Penanganan Pengaduan Masyarakat BOS : Home';
		
		$data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
		$data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
		$data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
		$data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
		
		$data['content'] = 'home';
		$this->load->view('home/main',$data);
		
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */