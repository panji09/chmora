<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_management extends CI_Controller {
    function __construct(){
        parent::__construct();
        parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
        $this->load->helper(array('url','date'));
        $this->load->model(array('Select_db','Insert_db','Update_db','Delete_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        $this->initlib->cek_session_handling();
    }
    function index(){
        $this->load->view('handling/main');
        
    }
    
}

?>