<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date','recaptchalib'));
		$this->load->model(array('Select_db','Insert_db','Update_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
	}
	
	public function index(){
		//$this->check_token($this->session->userdata('token'));
		redirect('registrasi/step1');
	}
	
	function step1(){
		$data['title']='Registrasi';
		//$this->load->view('home/second_view');
		//local
		//$data['publickey']='6LcNrc8SAAAAAK8ZXJjq-IL_O0KSs3vUMs8QNF_N'; 
		//bos.kemdiknas.go.id
		$data['publickey']='6LcJrc8SAAAAAEpxDuxrndytsgi3C9zpF_tYBATn';
		$this->load->view('home/registrasi_view',$data);
	}
	
	function post_step1(){
		//captcha
		//local
		//$privatekey='6LcNrc8SAAAAAPug8wOWIQstTwfu80sntgfgQ3vX';
		//bos.kemdiknas.go.id
		$privatekey='6LcJrc8SAAAAAOhFYtWJWq3A4ZuK_3gjV22WqOq0';
		$resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {
                //echo "You got it!";
				$get_token=$this->input->post('kode');
				//cek token
				$this->check_token($get_token);
				redirect('registrasi/step2');
        } else {
                # set the error code so that we can display it
                //echo $resp->error;
                $this->session->set_flashdata('error_captcha','CAPTCHA yang dimasukan salah!.');
		redirect('registrasi/step1');
        }
	}
	private function check_token($token){
		//cek token
		$data_token=$this->Select_db->t_user('by_token',array('token' => $token));
		if($data_token->num_rows()==1){
			$this->session->set_userdata('token',$token);
		}else{
			redirect('registrasi/step1');
		}
	}
	function step3(){
		//$this->check_token($this->session->userdata('token'));
		$t_user=$this->Select_db->t_user('by_token',array('token' => $this->session->userdata('token')));
		$data['t_user']=$t_user->row();
		$data['title']='Registrasi';
		$this->load->view('home/registrasi_view',$data);
	}
	function step2(){
		$this->check_token($this->session->userdata('token'));
		$t_user=$this->Select_db->t_user('by_token',array('token' => $this->session->userdata('token')));
		$data['t_user']=$t_user->row();
		$data['title']='Registrasi';
		$this->load->view('home/registrasi_view',$data);
	}
	function post_step2(){
		
		$nama=$this->input->post('nama');
		$nip=$this->input->post('nip');
		$jabatan_dinas=$this->input->post('dinas');
		$jabatan_bos=$this->input->post('tim_bos');
		$email=$this->input->post('email');
		$alamat_kantor=$this->input->post('alamat_kantor');
		$alamat_rumah=$this->input->post('alamat_rumah');
		$telp_kantor=$this->input->post('telp_kantor');
		$fax_kantor=$this->input->post('fax_kantor');
		$hp=$this->input->post('hp');
		
		$data=array(
			'nama' => $nama,
			'nip' => $nip,
			'jabatan_dinas' => $jabatan_dinas,
			'jabatan_bos' => $jabatan_bos,
			'email' => $email,
			'alamat_kantor' => $alamat_kantor,
			'telp_kantor' => $telp_kantor,
			'fax_kantor' => $fax_kantor,
			'alamat_rumah' => $alamat_rumah,
			'handphone' => $hp
			);
		$t_user=$this->Select_db->t_user('by_token',array('token' => $this->session->userdata('token')))->row();
		
		$this->Update_db->t_user($t_user->id, $data);
		
		//sent email
		/*
		$this->email->from('bos@kemdikbud.go.id', 'Bantuan Operasional Sekolah');
		$this->email->to($email);
		$this->email->subject('Aktivasi User P3M BOS');
		$this->email->message($this->template_email_aktivasi($t_user->username,base64_encode(base64_encode($this->session->userdata('token')))));
		if($this->email->send())
                $this->session->set_flashdata('valid',true);
        else
                $this->session->set_flashdata('valid',false);
		*/
		$msg=$this->template_email_aktivasi($t_user->username,base64_encode(base64_encode($this->session->userdata('token'))));
		$data_email=array(
		  'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
		  'to' => $email,
		  'subject' => 'Aktivasi User P3M BOS',
		  'body' => $msg
		);
		$this->Insert_db->send_email($data_email);
		$this->session->set_flashdata('valid',true);
		redirect('registrasi/step2');
	}
	function aktivasi($kode=null){
		if($kode==null){
			redirect('');
		}else{
			$kode=base64_decode(base64_decode($kode));
			$t_user=$this->Select_db->t_user('by_token',array('token' => $kode))->row();
			$this->Update_db->t_user($t_user->id, array('token' => ''));
			//sent email
			/*
			$this->email->from('bos@kemdikbud.go.id', 'Bantuan Operasional Sekolah');
			$this->email->to($t_user->email);
			$this->email->subject('Username & Password P3M BOS');
			$this->email->message($this->template_email_password($t_user->username,$t_user->userid,$t_user->pass_no_hash,'Username dan Password anda telah dibuat,'));
			if($this->email->send())
					$this->session->set_flashdata('valid',true);
			else
					$this->session->set_flashdata('valid',false);
			*/
			$msg=$this->template_email_password($t_user->username,$t_user->userid,$t_user->pass_no_hash,'Username dan Password anda telah dibuat,');
			$data_email=array(
			  'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
			  'to' => $t_user->email,
			  'subject' => 'Username & Password P3M BOS',
			  'body' => $msg
			);
			$this->Insert_db->send_email($data_email);
			
			$this->session->set_flashdata('valid',true);
			redirect('registrasi/step3');
		}
	}

	private function template_email_aktivasi($nama, $kode){
		return '
		<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="background-color: rgb(233, 239, 242); padding: 30px 15px 0pt;">
    <table width="710" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-size:16px;color:#333">
      <tbody><tr><td style="background-color:#025F99">
          <img width="710" height="88" style="border:0;display:block" src="'.$this->config->item('home_img').'/email_header.png" alt="BOS">
      </td>
      </tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 25px 40px 22px;">
<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 auto">
  <tbody><tr>
    <td width="540" valign="middle" height="36" style="padding:0 0 25px">
      <h1 style="font-weight:normal;font-size:19px;line-height:1.2;margin:0">
        
              <strong>Hai,</strong>
              <span style="color:#5a7b93">'.$nama.'</span></h1>
    </td>
    <td valign="middle" align="right" style="padding:0 0 25px">
      </td>
  </tr>
</tbody></table>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#ecf2f5;border-radius:4px">
  <tbody><tr>
    <td valign="top" style="padding:15px 10px 15px 15px">
         </td>

    <td width="100%" valign="top" style="padding: 15px 15px 15px 0pt;">
      <div style="line-height:15px">
      Untuk aktivasi silahkan copy link dibawah ini dan paste di url browser<br>
      <pre>
      ----------------------copy--------------------
      '.site_url('registrasi/aktivasi/'.$kode).'
      --------------------end copy------------------
      </pre>
      atau klik tautan dibawah ini </div>

      </td>
  </tr>
</tbody></table>
      </td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 10px;"></td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 32px 40px 22px; border-top: 1px solid rgb(228, 236, 240);">
  <table cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="middle" style="padding:0 0 0 15px">
      <table cellspacing="0" cellpadding="0" border="0" style="background-color:#025F99">
        <tbody><tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>

        <tr>
          <td></td>
          <td style="padding:5px">
            <a target="_blank" style="color:#fff;text-decoration:none;padding:5px 13px;font-size:16px;display:block" href="'.site_url('registrasi/aktivasi/'.$kode).'">
              Aktivasi</a>
          </td>
          <td></td>
        </tr>

        <tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td style="background-color:#4e6f8a"></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>
      </tbody></table>
    </td>
    <td valign="middle" style="padding:0 0 0 15px;font-size:15px;line-height:19px">
    </td>
    
  </tr>
</tbody></table>
      </td></tr>
      <tr><td style="padding: 0pt 0pt 10px; background-color: rgb(255, 255, 255); border-bottom-right-radius: 8px; border-bottom-left-radius: 8px;"></td></tr>
      <tr><td>
      <div style="padding:0 5px">
        <div style="min-height: 2px; line-height: 2px; font-size: 2px; background-color: rgb(226, 231, 231); clear: both; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;"></div>
      </div>
      </td></tr>
      <tr>
        <td style="font-size: 11px; line-height: 16px; color: rgb(170, 170, 170); padding: 25px 40px;">
	<font color="red">Perhatian: jika email ini masuk dalam spam, tolong tandai dengan <em>not spam</em>, dan tambahkan email bos@kemdikbud.go.id, bos.kemdikbud@gmail.com kedalam kontak anda</font> <br>
  Pesan ini berdasarkan permintaan anda di http://bos.kemdikbud.go.id, jika merasa tidak pernah mendaftar abaikan pesan ini.</td>
      </tr>
    </tbody></table>
  </td></tr></tbody></table>
		';
	}
	private function template_email_password($nama, $username, $password,$pesan){
		return '
		<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="background-color: rgb(233, 239, 242); padding: 30px 15px 0pt;">
    <table width="710" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-size:16px;color:#333">
      <tbody><tr><td style="background-color:#025F99">
          <img width="710" height="88" style="border:0;display:block" src="'.$this->config->item('home_img').'/email_header.png" alt="BOS">
      </td>
      </tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 25px 40px 22px;">
<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 auto">
  <tbody><tr>
    <td width="540" valign="middle" height="36" style="padding:0 0 25px">
      <h1 style="font-weight:normal;font-size:19px;line-height:1.2;margin:0">
        
              <strong>Hai,</strong>
              <span style="color:#5a7b93">'.$nama.'</span></h1>
    </td>
    <td valign="middle" align="right" style="padding:0 0 25px">
      </td>
  </tr>
</tbody></table>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#ecf2f5;border-radius:4px">
  <tbody><tr>
    <td valign="top" style="padding:15px 10px 15px 15px">
         </td>

    <td width="100%" valign="top" style="padding: 15px 15px 15px 0pt;">
		<div style="line-height:15px">'.$pesan.'</div>
		<div style="line-height:15px">&nbsp;</div>
		<div style="line-height:15px">Username : '.$username.'</div>
		<div style="line-height:15px">Password : '.$password.'</div>
		<div style="line-height:15px">&nbsp;</div>
		<div style="line-height:15px">Untuk memulai login, silahkan klik tautan dibawah ini</div>
      </td>
  </tr>
</tbody></table>
      </td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 10px;"></td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 32px 40px 22px; border-top: 1px solid rgb(228, 236, 240);">
  <table cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="middle" style="padding:0 0 0 15px">
      <table cellspacing="0" cellpadding="0" border="0" style="background-color:#025F99">
        <tbody><tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>

        <tr>
          <td></td>
          <td style="padding:5px">
            <a target="_blank" style="color:#fff;text-decoration:none;padding:5px 13px;font-size:16px;display:block" href="'.site_url('').'">
              Login</a>
          </td>
          <td></td>
        </tr>

        <tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td style="background-color:#4e6f8a"></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>
      </tbody></table>
    </td>
    <td valign="middle" style="padding:0 0 0 15px;font-size:15px;line-height:19px">
    </td>
    
  </tr>
</tbody></table>
      </td></tr>
      <tr><td style="padding: 0pt 0pt 10px; background-color: rgb(255, 255, 255); border-bottom-right-radius: 8px; border-bottom-left-radius: 8px;"></td></tr>
      <tr><td>
      <div style="padding:0 5px">
        <div style="min-height: 2px; line-height: 2px; font-size: 2px; background-color: rgb(226, 231, 231); clear: both; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;"></div>
      </div>
      </td></tr>
      <tr>
        <td style="font-size: 11px; line-height: 16px; color: rgb(170, 170, 170); padding: 25px 40px;">
  Pesan ini berdasarkan permintaan anda di http://bos.kemdikbud.go.id, jika merasa tidak pernah mendaftar abaikan pesan ini.</td>
      </tr>
    </tbody></table>
  </td></tr></tbody></table>
		';
	}
	/*
	function randomPrefix($length) { 
		$random= "";
		srand((double)microtime()*1000000);
		
		$data = "0123456789"; 
		//$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz"; 
		//$data .= "0FGH45OP89";
		
		for($i = 0; $i < $length; $i++) { 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}
		return $random; 
	}
 	
	function generate_token(){
		$t_user=$this->db->get('t_user');
		foreach($t_user->result() as $row){
			do{
				//$duplikat=true;
				$token=$this->randomPrefix(4);
				$this->db->where('token',$token);
				$cek_duplikat=$this->db->get('t_user');
				echo $token.' '.$cek_duplikat->num_rows().' ';
				if($cek_duplikat->num_rows()==0){
					//echo 'ui';
					$duplikat=false;
				}else{
					$duplikat=true;
				}
			}while($duplikat==true);
			
			echo $row->id.' '.$token."<br>";
			$this->db->where('id',$row->id);
			$this->db->update('t_user', array('token' => $token));
			//sleep(1);
		}
	}
	*/
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */