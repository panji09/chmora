    <?php
	$halaman = $this->uri->segment(2);
	
    ?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
		    <?php
			$list_halaman = array(
			    'berita' => 'Berita',
			    'pengumuman' => 'Pengumuman',
			    'peraturan' => 'Peraturan Perundangan',
			    'publikasi' => 'Publikasi'
			);
		    ?>
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><a href="<?=site_url('halaman')?>">Halaman</a></h6>
                    <h6><a href="<?=site_url('halaman/'.$halaman)?>"><?=$list_halaman[$halaman]?></a></h6>
                    <h6><span class="page-active"><?=$halaman_main->title?></span></h6>
                    
                    
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <div class="blog-post-container">
                            
                            <div class="blog-post-inner">
                                <h3 class="blog-post-title"><?=$halaman_main->title?></h3>
                                <?=$halaman_main->content?>
                                <div id="sharethis">
				    <span class='st_facebook_vcount' displayText='Facebook'></span>
				    <span class='st_twitter_vcount' displayText='Tweet'></span>
				    <span class='st_googleplus_vcount' displayText='Google +'></span>
				    <span class='st_email_vcount' displayText='Email'></span>
				</div>
                            </div>
                        </div> <!-- /.blog-post-container -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.col-md-8 -->
            <!--./main content-->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">
		
		<?php if($halaman != 'berita'):?>
		<!-- show berita -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Berita</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($berita->num_rows()) foreach($berita->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='#'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show berita -->
		<?php endif;?>
		
		<?php if($halaman != 'pengumuman'):?>
		<!--show pengumuman-->
                <div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Pengumuman</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($pengumuman->num_rows()) foreach($pengumuman->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='#'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!--./show pengumuman-->
		<?php endif;?>
		
		<?php if($halaman != 'peraturan'):?>
		<!-- show peraturan perundangan-->
                <div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Peraturan Perundangan</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($peraturan->num_rows()) foreach($peraturan->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='#'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show peraturan perundangan-->
		<?php endif;?>
		
		<?php if($halaman != 'publikasi'):?>
		<!-- show publikasi -->
		<div class="widget-main">
		    <div class="widget-main-title">
			<h4 class="widget-title">Publikasi</h4>
		    </div> <!-- /.widget-main-title -->
		    <div class="widget-inner">
			<?php if($publikasi->num_rows()) foreach($publikasi->result() as $row):?>
			<?php  if($row->published):?>
			    <div class="blog-list-post clearfix">
				<?php 
				/*
				<div class="blog-list-thumb">
				    <a href="blog-single.html"><img src="http://placehold.it/65x65" alt=""></a>
				</div>
				*/
				?>
				
				<div class="blog-list-details">
				    <h5 class="blog-list-title"><a href="blog-single.html"><?=$row->title?></a></h5>
				    <p class="blog-list-meta small-text"><span><a href="#"><?=$row->last_update?></a></span> <!--with <span><a href="#">3 comments</a></span>--></p>
				</div>
			    </div> <!-- /.blog-list-post -->
			<?php endif;?>
			<?php endforeach;?>
			<div class="blog-list-post clearfix">
			    <p class="text-right"><a href='#'>Index</a></p>
			</div>
		    </div> <!-- /.widget-inner -->
		</div> <!-- /.widget-main -->
		<!-- ./show publikasi -->
		<?php endif;?>
            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
