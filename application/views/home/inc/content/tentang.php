    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="<?=site_url('')?>">Home</a></h6>
                    <h6><span class="page-active">Tentang</span></h6>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="course-post">
                            <div class="course-details clearfix">
                                <h3 class="course-post-title"><?=$tentang->title?></h3>
                                <?=$tentang->content?>
                                
                                <div id="sharethis">
				    <span class='st_facebook_vcount' displayText='Facebook'></span>
				    <span class='st_twitter_vcount' displayText='Tweet'></span>
				    <span class='st_googleplus_vcount' displayText='Google +'></span>
				    <span class='st_email_vcount' displayText='Email'></span>
				</div>
                            </div> <!-- /.course-details -->
                        </div> <!-- /.course-post -->

                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->


                
            </div> <!-- /.col-md-8 -->


            
    
        </div> <!-- /.row -->
    </div> <!-- /.container --> 
