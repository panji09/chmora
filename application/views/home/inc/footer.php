    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Hubungi Kami</h4>
                        <p>Pusat Informasi dan Hubungan Masyarakat <br>Jalan Lapangan Banteng Barat No. 3 - 4 Jakarta 10710 <br>Telepon: (+6221) - 3811679 - 34833004</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Tautan Pendukung</h4>
                        <ul class="list-links">
                            <li>Kementerian Agama RI - <a href="#">http://www.kemenag.go.id/</a></li>
                            
                        </ul>
                    </div>
                </div>
                
                
            </div> <!-- /.row -->

            <div class="bottom-footer">
                <div class="row">
                    <div class="col-md-5">
                        <p class="small-text">&copy; Copyright 2014. Kementerian Agama. All Rights Reserved.</p>
                    </div> <!-- /.col-md-5 -->
                    
                </div> <!-- /.row -->
            </div> <!-- /.bottom-footer -->

        </div> <!-- /.container -->
    </footer> <!-- /.site-footer --> 
