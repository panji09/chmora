    <header class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 header-left logo">
                        <a href="index.html" title="Universe" rel="home">
                            <img src="<?=$this->config->item('home_img')?>/logo_bos.png" alt="bos">
                        </a>
                    
                </div> <!-- /.header-left -->

                <div class="col-md-10 page-header">
		    <h1>Penanganan Pengaduan Masyarakat BOS <br>Kementerian Agama</h1>
                </div> <!-- /.col-md-4 -->

                
            </div>
        </div> <!-- /.container -->

        <div class="nav-bar-main" role="navigation">
            <div class="container">
                <nav class="main-navigation clearfix visible-md visible-lg" role="navigation">
                        <ul class="main-menu sf-menu">
			    <?php $page = $this->uri->segment(1);?>
			    <li><a href="http://pendis.kemenag.go.id/simbos/">SIMBOS</a></li>
                            <li <?=($page == '' ? 'class="active"' : '')?>><a href="<?=site_url('')?>">Home</a></li>
                            <li <?=($page == 'statistik_pengaduan' ? 'class="active"' : '')?>>
				<a href="<?=site_url('statistik_pengaduan')?>">Statistik Pengaduan</a>
                            </li>
                            <li <?=($page == 'lihat_pengaduan' ? 'class="active"' : '')?>>
				<a href="<?=site_url('lihat_pengaduan')?>">Lihat Pengaduan</a>
                            </li>
                            <li <?=($page == 'pengaduan' ? 'class="active"' : '')?>>
				<a href="<?=site_url('pengaduan')?>">Pengaduan Online <span id='unapproved' style='display:none' class="badge">0</span></a>
                            </li>
                            <li <?=($page == 'faq' ? 'class="active"' : '')?>>
				<a href="<?=site_url('faq')?>">FAQ</a>
                            </li>
                            <li <?=($page == 'kontak' ? 'class="active"' : '')?>><a href="<?=site_url('kontak')?>">Kontak</a></li>
                        </ul> <!-- /.main-menu -->

                        
                </nav> <!-- /.main-navigation -->
            </div> <!-- /.container -->
        </div> <!-- /.nav-bar-main -->

    </header> <!-- /.site-header --> 
