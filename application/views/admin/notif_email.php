<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td style="background-color: rgb(233, 239, 242); padding: 30px 15px 0pt;">
    <table width="710" cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;font-size:16px;color:#333">
      <tbody><tr><td style="background-color:#025F99">
          <img width="710" height="88" style="border:0;display:block" src="<?=$this->config->item('home_img')?>/email_header.png" alt="BOS">
      </td>
      </tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 25px 40px 22px;">
<table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 auto">
  <tbody><tr>
    <td width="540" valign="middle" height="36" style="padding:0 0 25px">
      <h1 style="font-weight:normal;font-size:19px;line-height:1.2;margin:0">
        
              <strong>Hai,</strong>
              <span style="color:#5a7b93"><?=$user->nama?></span> <br />
              ada <b><?=($not_approve->num_rows()+$not_approve_sms->num_rows())?></b> Pengaduan  yang belum di verifikasi </h1>
    </td>
    <td valign="middle" align="right" style="padding:0 0 25px">
      </td>
  </tr>
</tbody></table>
<?php 
if($not_approve->result()!=0):
foreach($not_approve->result() as $row):?>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#ecf2f5;border-radius:4px">
  <tbody><tr>
    <td valign="top" style="padding:15px 10px 15px 15px">
         </td>

    <td width="100%" valign="top" style="padding: 15px 15px 15px 0pt;">
      <div style="line-height:15px"><font size="3">Di Posting <?=mysqldatetime_to_date_id($row->tanggal)?> Oleh <?=($row->tampil_nama=='1' ? $row->nama : 'xxx')?>, <?=($row->tampil_alamat=='1' ? $row->alamat : 'xxx')?>, <?=($row->tampil_telp=='1' ? $row->telp : 'xxx')?><br /><?=$row->sekolah?>, <?=$row->lokasi?></font><br /><hr /><?=$this->initlib->shorter($row->deskripsi,400,'<a target="_blank" href="'.site_url('administrator').'"> [selengkapnya..]</a>')?></div>
	  
      </td>
  </tr>
</tbody></table><br />
<?php 
endforeach;
endif;?>

<?php 
if($not_approve_sms->result()!=0):
foreach($not_approve_sms->result() as $row):?>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#ecf2f5;border-radius:4px">
  <tbody><tr>
    <td valign="top" style="padding:15px 10px 15px 15px">
         </td>

    <td width="100%" valign="top" style="padding: 15px 15px 15px 0pt;">
      <div style="line-height:15px"><font size="3">Di Posting <?=mysqldatetime_to_date_id($row->tanggal)?> Oleh <?=substr_replace($row->msisdn, 'xxx', -3, 3)?><br />
          <?=$row->sekolah?>, <?=$row->lokasi?></font><br /><hr /><?=$this->initlib->shorter($row->deskripsi,400,'<a target="_blank" href="'.site_url('administrator').'"> [selengkapnya..]</a>')?></div>
	  
      </td>
  </tr>
</tbody></table><br />
<?php 
endforeach;
endif;?>
      </td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 10px;"></td></tr>
      <tr><td style="background-color: rgb(255, 255, 255); padding: 32px 40px 22px; border-top: 1px solid rgb(228, 236, 240);">
  <table cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td valign="middle" style="padding:0 0 0 15px">
      <table cellspacing="0" cellpadding="0" border="0" style="background-color:#025F99">
        <tbody><tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>

        <tr>
          <td></td>
          <td style="padding:5px">
            <a target="_blank" style="color:#fff;text-decoration:none;padding:5px 13px;font-size:16px;display:block" href="<?=site_url('administrator')?>">
              Masuk</a>
          </td>
          <td></td>
        </tr>

        <tr>
          <td width="1" height="1" style="background-color:#fff"></td>
          <td style="background-color:#4e6f8a"></td>
          <td width="1" height="1" style="background-color:#fff"></td>
        </tr>
      </tbody></table>
    </td>
    <td valign="middle" style="padding:0 0 0 15px;font-size:15px;line-height:19px">
    </td>
    
  </tr>
</tbody></table>
      </td></tr>
      <tr><td style="padding: 0pt 0pt 10px; background-color: rgb(255, 255, 255); border-bottom-right-radius: 8px; border-bottom-left-radius: 8px;"></td></tr>
      <tr><td>
      <div style="padding:0 5px">
        <div style="min-height: 2px; line-height: 2px; font-size: 2px; background-color: rgb(226, 231, 231); clear: both; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px;"></div>
      </div>
      </td></tr>
      <tr>
        <td style="font-size: 11px; line-height: 16px; color: rgb(170, 170, 170); padding: 25px 40px;">
  Pesan ini adalah pesan otomatis berdasarkan permintaan anda di http://bos.kemdikbud.go.id/pengaduan</td>
      </tr>
    </tbody></table>
  </td></tr></tbody></table>