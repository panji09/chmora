<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pengaduan Online Admin</title>
<link rel="icon" type="image/png" href="<?=$this->config->item('home_img')?>/favicon.png" />

<?php 
//setting css page home
if($this->uri->segment(2)=="index" || $this->uri->segment(2)=="" || $this->uri->segment(2)=="sms" || $this->uri->segment(2)=="user_handling"):?> 
	<link rel="stylesheet" href="<?=$this->config->item('admin_plugin')?>/datatables/media/css/demo_table.css" type="text/css" />
<?php endif;
//end setting css page home
?>

<link rel="stylesheet" href="<?=$this->config->item('admin_css')?>/screen.css" type="text/css"  />

<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script type="text/javascript" src="<?=$this->config->item('admin_js')?>/jquery-1.7.1.min.js"></script>
<!--jquery ui-->
<link rel="stylesheet" href="<?=$this->config->item("admin_js")?>/jquery-ui/themes/redmond/jquery.ui.all.css">
<script src="<?=$this->config->item("admin_js")?>/jquery-ui/ui/jquery-ui-1.8.18.custom.js"></script>
<!--end jquery ui-->


<?php 
// setting js page ALL
?>
<!--  checkbox styling script -->
<script src="<?=$this->config->item('admin_js')?>/jquery/ui.core.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/jquery/ui.checkbox.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.bind.js" type="text/javascript"></script>

<![if !IE 7]>

<!--  styled select box script version 1 -->
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.selectbox-0.5.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect').selectbox({ inputClass: "selectbox_styled" });
});
</script>
 

<![endif]>

<!--  styled select box script version 2 --> 
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
	$('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>

<!--  styled select box script version 3 --> 
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.selectbox-0.5_style_2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" } );
	$('.styledselect_pages').change(function() {
  alert('Handler for .change() called.');
});
	//$('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" }).change(function{(alert('ui'))});
});
</script>

<!--  styled file upload script --> 
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.filestyle.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
  $(function() {
      $("input.file_1").filestyle({ 
          image: "<?=$this->config->item('admin_img')?>/forms/choose-file.gif",
          imageheight : 21,
          imagewidth : 78,
          width : 310
      });
  });
</script>

<!-- Custom jquery scripts -->
<script src="<?=$this->config->item('admin_js')?>/jquery/custom_jquery.js" type="text/javascript"></script>
 
<!-- Tooltips -->
<!--<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.dimensions.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	$('a.info-tooltip ').tooltip({
		track: true,
		delay: 0,
		fixPNG: true, 
		showURL: false,
		showBody: " - ",
		top: -35,
		left: 5
	});
});
</script> -->


<!--  date picker script -->
<link rel="stylesheet" href="css/datePicker.css" type="text/css" />
<script src="<?=$this->config->item('admin_js')?>/jquery/date.js" type="text/javascript"></script>
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
	.datePicker(
		// associate the link with a date picker
		{
			createButton:false,
			startDate:'01/01/2005',
			endDate:'31/12/2020'
		}
	).bind(
		// when the link is clicked display the date picker
		'click',
		function()
		{
			updateSelects($(this).dpGetSelected()[0]);
			$(this).dpDisplay();
			return false;
		}
	).bind(
		// when a date is selected update the SELECTs
		'dateSelected',
		function(e, selectedDate, $td, state)
		{
			updateSelects(selectedDate);
		}
	).bind(
		'dpClosed',
		function(e, selected)
		{
			updateSelects(selected[0]);
		}
	);
	
var updateSelects = function (selectedDate)
{
	var selectedDate = new Date(selectedDate);
	$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
	$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
	$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
	.bind(
		'change',
		function()
		{
			var d = new Date(
						$('#y').val(),
						$('#m').val()-1,
						$('#d').val()
					);
			$('#date-pick').dpSetSelected(d.asString());
		}
	);

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>

<script>
function reload_page(){
			//alert('hai ini dari parent');
			document.location.reload(true);
	}
$(document).ready(function(){
	
	$('#list_action').change(function(){
		$('#mainform').submit();
	});
	$('#num_rows').change(function(){
		$('#form_num_rows').submit();
	});
	
});

</script>

<?php 
// setting js page ALL
?>
<!--fancybox-->
<script type="text/javascript" src="<?=$this->config->item('admin_plugin')?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="<?=$this->config->item('admin_plugin')?>/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="<?=$this->config->item('admin_plugin')?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<link rel="stylesheet" href="<?=$this->config->item('admin_plugin')?>/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<!--end fancybox-->

<script type="text/javascript" language="javascript" src="<?=$this->config->item('admin_plugin')?>/datatables/media/js/jquery.dataTables.js"></script>
<?php 
//setting js page home
if($this->uri->segment(2)=="index" || $this->uri->segment(2)=="" || $this->uri->segment(2)=="sms" ):
//ganti url ajax table
if($this->uri->segment(2)=="index" || $this->uri->segment(2)==""){
	$ajax_table=site_url('administrator/ajax_ponline');
}elseif($this->uri->segment(2)=="sms"){
	$ajax_table=site_url('administrator/ajax_psms');
}

?> 
	
	<!-- datatables-->
	
	<script type="text/javascript" charset="utf-8">
		var asInitVals = new Array();
		var i=0;
		var update=true;
		$(document).ready(function() {
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
			  return {
				"iStart":         oSettings._iDisplayStart,
				"iEnd":           oSettings.fnDisplayEnd(),
				"iLength":        oSettings._iDisplayLength,
				"iTotal":         oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
				"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
			  };
			}
			var oTable = $('#example').dataTable( {
				"bStateSave": false,
				"bProcessing": true,
				"bServerSide": true,
				"iDisplayLength": 25,
				"sPaginationType": "full_numbers",
				"sAjaxSource": "<?=$ajax_table?>",
				"sServerMethod": "POST",
				"aoColumnDefs": [ 
				 /* { "bSortable": false, "aTargets": [ 0 ] }, //disable soft kolom 1*/
				  { "bSortable": false, "aTargets": [ 0 ] }, //disable soft kolom 2/no
				  { "bSortable": false, "aTargets": [ 8 ] }, //disable soft kolom 10/action
				  {
					"fnRender": function ( oObj ) {
						if(update){
							i=oTable.fnPagingInfo().iStart;
							update=false;
						}
						if(i+1 == oTable.fnPagingInfo().iEnd){
							update=true;
						}
						//alert(i+' '+oTable.fnPagingInfo().iEnd);
						return ++i;
						
					},
					"bUseRendered": false,
					"aTargets": [ 0 ] //no
				  }
				],
				"fnDrawCallback": function () {
					$("a.iframe").fancybox({
						'width'				: '75%',
						'height'			: '100%',
						'autoScale'			: false,
						'transitionIn'		: 'none',
						'transitionOut'		: 'none',
						'type'				: 'iframe'
					});
					$('div.expandable p').expander({
						slicePoint:       250,  // default is 100
						expandPrefix:     ' ', // default is '... '
						expandText:       '[...]', // default is 'read more'
						collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
						userCollapseText: '[^]'  // default is 'read less'
					});
					$(".confirm_terima").click(function(e) {
						e.preventDefault();
						var id = $(this).attr("data-id");
						$("#dialog-ui-confirm_publish").dialog({
							buttons : {
								"Ya" : function() {
								  window.location.href = '<?=site_url('administrator/post_table/terima')?>/'+id;
								},
								"Tidak" : function() {
								  window.location.href = '<?=site_url('administrator/post_table/terima_hide')?>/'+id;
								}
							}
						});
						$("#dialog-ui-confirm_publish").dialog("open");
					});
					$(document).ready(dialogForms);
					var dialog=null;
					var pos=null;
					function dialogForms() {
						
					  $('a.dialog-form').click(function() {
					  	nTr = this.parentNode;
					  	pos = oTable.fnGetPosition( nTr.parentNode );
					  	var a = $(this);
						$.get(a.attr('href'),function(resp){
						  dialog = $('<div>').attr('id','formDialog').html($(resp).find('form:first').parent('div').html());
						  $('body').append(dialog);
						  dialog.find(':submit').hide();
						  dialog.dialog({
							title: a.attr('title') ? a.attr('title') : '',
							modal: true,
							buttons: {
							  'Save': function() {submitFormWithAjax($(this).find('form'));},
							  'Cancel': function() {$(this).dialog('close');}
							},
							close: function() {$(this).remove();},
							width: 'auto'
						  });
						}, 'html');
						return false;
					  });
					  function submitFormWithAjax(form) {
					  form = $(form);
					  
					  var respon=$.ajax({
						url: form.attr('action'),
						data: form.serialize(),
						type: (form.attr('method')),
						dataType: 'html',
						success : function(respon) {  
							if(respon=="sukses"){
								//delete row di browser
								
								oTable.fnDeleteRow( pos );
								dialog.dialog("close");
							}else
								alert("Data gagal dimasukkan.");
						},  
						error : function() {  
							alert("request error");  
						}  
					  });
					  
					  return false;
					}
					}
					$( ".datepicker" ).datepicker();
					$( ".datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
				}
			} );
			
			oTable.fnSort( [ [1,'asc'],[2,'desc'] ] ); //sort asc status & tanggal
			$("#example tfoot input").change( function () {
				/* Filter on the column (the index) of this element */
				var id = $(this).attr('id').split("-")[1];
				oTable.fnFilter( this.value, id );
			} );
			$("#example tfoot input").keyup( function () {
				/* Filter on the column (the index) of this element */
				var id = $(this).attr('id').split("-")[1];
				oTable.fnFilter( this.value, id );
			} );
			 
			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
			 * the footer
			 */
			$("tfoot input").each( function (i) {
				asInitVals[i] = this.value;
			} );
			 
			/*$("tfoot input").focus( function () {
				if ( this.className == "search_init" )
				{
					this.className = "";
					this.value = "";
				}
			} );*/
			 
			/*$("tfoot input").blur( function (i) {
				if ( this.value == "" )
				{
					this.className = "search_init";
					this.value = asInitVals[$("tfoot input").index(this)];
				}
			} );*/
			
			
		} );
	</script>
	<!-- end datatables-->
	
	<!--jquery expander-->
	<script src="<?=$this->config->item('admin_js')?>/jquery.expander.js"></script>
	<!--end jquery expander-->
<?php endif;
// end setting js page home
?>

<?php
//setting js page user handling
$ajax_table=site_url('administrator/ajax_user_handling');
if($this->uri->segment(2)=='user_handling'):?>
	<script type="text/javascript" charset="utf-8">
		var asInitVals = new Array();
		var i=0;
		var update=true;
		$(document).ready(function() {
			$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
			{
			  return {
				"iStart":         oSettings._iDisplayStart,
				"iEnd":           oSettings.fnDisplayEnd(),
				"iLength":        oSettings._iDisplayLength,
				"iTotal":         oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
				"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
			  };
			}
			var oTable = $('#example').dataTable( {
				"bStateSave": false,
				"bProcessing": true,
				"bServerSide": true,
				"iDisplayLength": 25,
				"sPaginationType": "full_numbers",
				"sAjaxSource": "<?=$ajax_table?>",
				"sServerMethod": "POST",
				"aoColumnDefs": [ 
				{
					"fnRender": function ( oObj ) {
						if(update){
							i=oTable.fnPagingInfo().iStart;
							update=false;
						}
						if(i+1 == oTable.fnPagingInfo().iEnd){
							update=true;
						}
						//alert(i+' '+oTable.fnPagingInfo().iEnd);
						return ++i;
						
					},
					"bUseRendered": false,
					"aTargets": [ 0 ] //no
				  }
				],
				"fnDrawCallback": function () {
					
				}
			} );
			
			
		} );
	</script>

<?php endif;
//setting js page user handling
?>
<?php
//setting js page berita
if($this->uri->segment(2)=="berita" || $this->uri->segment(2)=="faq" || $this->uri->segment(2)=="artikel" || $this->uri->segment(2)=="gallery"):?>
	<script type="text/javascript" src="<?=$this->config->item('admin_plugin')?>/ckeditor/ckeditor.js"></script>
	<script>
	$(document).ready(function(){
		$(".fancy_img").fancybox();
	});
	</script>
<?php endif;
//end setting js page berita
?>
<?php
//setting js page analytics
if($this->uri->segment(2)=="analytics"):?>
	
<?php endif;
//end setting js page analytics

//setting js page edit faq
if($this->uri->segment(2)=="faq" && $this->uri->segment(3)=="edit"):?>
	<script>
	$(document).ready(function(){
		var textarea = $("textarea");
		function refresh_ckeditor(){
				CKEDITOR.config.toolbar = 'Basic';
				
				for(name in CKEDITOR.instances){
					try{
						CKEDITOR.instances[name].destroy();
					}catch(err){
					}
					
					//alert(name);
				}
				CKEDITOR.replaceAll();
		  	
			
		}
		
		function hapus(){
			$(".hapus").click(function(){
				$id_del=$(this).attr("id");
				
				$id_faq=$("#id_faq"+$id_del).val();
				if($id_faq!=""){
					<?php $url=site_url('services/delete_faq');?>
					$.get('<?=$url?>/'+$id_faq);
				}
				//alert('id='+$id_del+'id_faq='+$id_faq);
				try{
					CKEDITOR.instances["tanya"+$id_del].destroy();
					CKEDITOR.instances["jawab"+$id_del].destroy();
				}catch(err){
				}
				
				
				$("#a"+$id_del).remove();
				$("#q"+$id_del).remove();
				
				
				
			});
			
		}
		hapus();
		function show_faq(){
			$id_kat=$('#faq_kategori').val();
			//alert($id_kat);
			
			<?php $url=site_url('services/get_table_faq');?>
			$.get('<?=$url?>/'+$id_kat, function(data) {
				$('#id-form tbody').html(data);
				hapus();
				refresh_ckeditor();
			});
			
		}
		show_faq();
		function show_tambah(){
			if($("#faq_kategori").val()=="tambah"){
				$("#tambah_faq_kat").show(); 
				$("#edit_faq_kat").hide();
			}else{
				$("#tambah_faq_kat").hide();
				$("#edit_faq_kat").show();
			}
		}
		show_tambah();
		$("#faq_kategori").change(function () {
			show_tambah();
		});

		$("#tambah").click(function(){
			$jumlah_qa=($('#id-form >tbody tr').length/2)+1;
			//alert($jumlah_qa);
			$no_qa=1;
			//alert($("#id-form >tbody >tr:last").attr("id"));
			if($jumlah_qa > 1)
				$no_qa=parseInt($("#id-form >tbody >tr:last").attr("id").substring(1,2))+1;
			//alert($no_qa);
			if($jumlah_qa != $no_qa)
				$id=$no_qa;
			else
				$id=$jumlah_qa;
			$('#id-form >tbody:last').append('<tr id=a'+$id+'><td valign="top">'+$id+'.</td><td valign="top">Pertanyaan:</td><td><textarea name="tanya[]" cols="100" rows="3" id="tanya'+$id+'"></textarea><input type="hidden" name="id_faq[]" value="" id="id_faq'+$id+'"/></td><td valign="top"><input type="button" value="hapus" class="hapus styled-button-2" id="'+$id+'"/></td></tr><tr id=q'+$id+'> <td valign="top">&nbsp;</td><td valign="top">Jawaban:</td><td><textarea name="jawab[]" cols="100" rows="5" id="jawab'+$id+'"></textarea></td><td>&nbsp;</td></tr>');
			
			hapus();
			refresh_ckeditor();
		});
		
		
		
		$('#faq_kategori').change(function() {
			show_faq();
		});
	});
	</script>
<?php endif;
//end setting js page faq

//setting js page tambah gallery
if($this->uri->segment(2)=="gallery" && $this->uri->segment(3)=="tambah"):?>
	<script>
	$(document).ready(function(){
		var textarea = $("textarea");
		function refresh_ckeditor(){
				CKEDITOR.config.toolbar = 'Minimal';
				
				for(name in CKEDITOR.instances){
					try{
						CKEDITOR.instances[name].destroy();
					}catch(err){
					}
					
					//alert(name);
				}
				CKEDITOR.replaceAll();
		  	
			
		}
		
		/* This is basic - uses default settings */
	
		
	$("#upload").click(function(){
		openKCFinder();
	})
		
	function openKCFinder() {
		window.KCFinder = {
			callBackMultiple: function(url) {
				window.KCFinder = null;
				//textarea.value = "";
				<?php $url=site_url('services/create_table_gallery');?>
				
				$.ajax({
					async: false,
					type: 'GET',
					url: '<?=$url?>/'+url.length,
					success: function(data) {
					$('#id-form').html(data);
					}
				});
				for (var i = 0; i < url.length; i++){
					url_img=url[i];
					index=i+1;
					$("#gambar_href"+index).attr("href", url_img);
					$("#gambar_src"+index).attr("src", url_img);
					$("#gambar_url"+index).attr("value", url_img);
				}
				$(".fancy_img").fancybox();
				refresh_ckeditor()
			}
		};
		//http://www.bsm.site/media/admin/plugin/kcfinder/browse.php?type=files&CKEditor=ringkasan&CKEditorFuncNum=1&langCode=en
		window.open('<?=site_url('media/admin/plugin/kcfinder/browse.php?type=images&dir=images/gallery')?>',
			'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
			'directories=0, resizable=1, scrollbars=0, width=800, height=600'
		);
	}
});
	</script>
<?php endif;
// end //setting js page edit gallery

?>
<style>
.nav {
    width: auto !important;
}
</style>
</head>