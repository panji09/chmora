<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	<!--  start top-search -->
	<div id="top-search">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><!--<input type="text" value="Search" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" />--></td>
		<td>
		<!--<select  class="styledselect">
			<option value=""> All</option>
			<option value=""> Products</option>
			<option value=""> Categories</option>
			<option value="">Clients</option>
			<option value="">News</option>
		</select>--> 
		</td>
		<td>
		<!--<input type="image" src="images/shared/top_search_btn.gif"  />-->
		</td>
		</tr>
		</table>
	</div>
 	<!--  end top-search -->
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>FAQ</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			<?php if($this->uri->segment(3)=="edit"):?>
				<style>
			#id-form th{
				width:0;
				min-width:0;
			}
			#id-form td {
				padding: 10px 0 0 10px;
			}
			</style>
			<form action="<?=site_url('administrator/post_faq')?>" method="post">
		<table cellspacing="0" cellpadding="0" border="0" id="id-form">
		<thead>
		  <tr>
		    <th valign="top" style="width:20px">&nbsp;</th>
            <th valign="top">Kategori:</th>
		    <td>
				<select name="faq_kategori" id="faq_kategori">
					<option value="tambah" selected="selected">**Tambah Baru**</option>
					<?php foreach($faq_kategori->result() as $row):?>
					<option value="<?=$row->id?>"><?=$row->faq_kategori?></option>
					<?php endforeach;?>
				</select>
				<input type="text" name="tambah_faq_kat" id="tambah_faq_kat" />
				<div id="edit_faq_kat" style="display:inline">
				<a href="#" id="confirmLink-kat_faq_edit">Edit</a> | <a href="#" id="confirmLink-kat_faq_hapus">Hapus</a>
				</div>
				</td>
		    <td></td>
		    </tr>
		</thead>
		<tbody>
		<tr id="a1">
		  	<td valign="top">1.</td>
			<td valign="top">Pertanyaan:</td>
			<td><textarea name="tanya[]" cols="100" rows="3" id="tanya1" ></textarea> <input type="hidden" name="id_faq[]" value="" id="id_faq1" /> </td>
			<td valign="top"><input type="button" value="hapus" class="hapus styled-button-2" id="1"/></td>
		</tr>
		<tr id="q1">
		  <td valign="top">&nbsp;</td>
			<td valign="top">Jawaban:</td>
			<td><textarea name="jawab[]" cols="100" rows="5" id="jawab1"></textarea></td>
			<td>&nbsp;</td>
		</tr>
		</tbody>
		<tfoot>
	<tr>
	  <td>&nbsp;</td>
		<td>&nbsp;</td>
		<td valign="top">
			<input type="button" value="tambah" id="tambah" class="styled-button-2"> <input type="submit" class="styled-button-2" value="Submit">		</td>
		<td></td>
	</tr>
	</tfoot>
	</table>
	</form>
			<?php elseif($this->uri->segment(3)==""):?>
				<style type="text/css">
			.faqsection {
				margin: 10px 0;
			}
			
			div.faq {
				margin: 10px 0 0 10px;
			}
			
			div.faq .question {
				color: #2763A5;
				cursor:  pointer;
				padding-left: 10px;
				background: url(raquo.gif) no-repeat left 5px;
			}
			
			div.faq .question.active {
				background-image: url(raquo-down.gif);
			}
			
			div.faq .answer  {
				margin-left: 10px;	
			}
			
			div.faq ul,
			div.faq ol {
				margin: 0 0 10px 20px;
			}
			</style>
			<script>
			
			SSS_faq = {
			init : function() {
			$('div.faq .answer').not(':first').slideToggle('fast');
			$('div.faq .question').click(function() { SSS_faq.toggle(this) });
			},
			toggle : function(elt) {
			$(elt).toggleClass('active');
			$(elt).siblings('.answer').slideToggle('fast');
			}
			}
			$(function() {
			SSS_faq.init();
			});
			
			</script>
				<?php foreach($faq_kategori->result() as $row):?>
					<h2><span style="color:#000000;" class="western"><?=$row->faq_kategori?></span></h2>
					<?php 
					$faq=$this->Select_db->faq('by_kategori',array('id_kategori' => $row->id));
					foreach($faq->result() as $row_faq):
					?>
					<div class="faq">
						<div class="question"><?=$row_faq->tanya?></div>
						<div class="answer"><?=$row_faq->jawab?></div>
					</div>
					<?php endforeach;?>
				<?php endforeach;?>
			
			<?php endif;?>
			</div>
			<!--  end table-content  -->
	
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>

<!-- end footer -->

</body>
</html>