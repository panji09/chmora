<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/graph/r1/all/all/all')?>" class="current">Statistik Pengaduan</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" />Statistik Pengaduan
			</div>
	
		    <?php
		$kategori=array(
					'r1' => 'Kategori',
					'r2' => 'Sumber Informasi',
					'r3' => 'Jumlah & Status',
					'r4' => 'Kategori & Status',
					'r5' => 'Pelaku & Status',
					'r6' => 'Status Provinsi',
					'r7' => 'Media'
				);
		?>
		<h1>Pengaduan Berdasarkan <?=$kategori[$this->uri->segment(3)]?></h1>
		<div style="clear: both;"></div>
		<style>
		.chart_table table{
			border-collapse:collapse;
		}
		.chart_table table,.chart_table td,.chart_table th{
			border:1px solid black;
			color:#000000;
		}
		
		</style>
		<?php $t_tot=0;?>
		<?php $this->load->view('home/inc/chart/'.$this->uri->segment(3))?>
		<div style="clear: both;"></div><br>
		<?php include('inc/share_button.php');?>
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
     <?php include('inc/menu_kanan.php');?>
	  <!-- sidebar end -->
      <div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
</script>
</body>
</html>