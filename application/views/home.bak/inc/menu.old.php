<style>

nav {
	/*margin: -27px auto;*/
	position:relative;
	top: 0px; 
	text-align: center;
	width:1000px;
}

nav ul ul {
	display: none;
}

	nav ul li:hover > ul {
		display: block;
	}


nav ul {
	background: #efefef; 
	background: linear-gradient(top, #efefef 0%, #bbbbbb 100%);  
	background: -moz-linear-gradient(top, #efefef 0%, #bbbbbb 100%); 
	background: -webkit-linear-gradient(top, #efefef 0%,#bbbbbb 100%); 
	box-shadow: 0px 0px 9px rgba(0,0,0,0.15);
	padding: 0 20px;
	border-radius: 10px;  
	list-style: none;
	position: relative;
	display: inline-table;
}
	nav ul:after {
		content: ""; clear: both; display: block;
	}

	nav ul li {
		float: left;
	}
		.active, nav ul li:hover {
			background: #4b545f;
			background: linear-gradient(top, #4f5964 0%, #5f6975 40%);
			background: -moz-linear-gradient(top, #4f5964 0%, #5f6975 40%);
			background: -webkit-linear-gradient(top, #4f5964 0%,#5f6975 40%);
		}
			.active, nav ul li:hover a {
				color: #fff;
			}
		
		nav ul li a {
			display: block; padding: 20px 30px;
			color: #757575; text-decoration: none;
		}
			
		
	nav ul ul {
		background: #5f6975; border-radius: 0px; padding: 0;
		position: absolute; top: 100%;
	}
		nav ul ul li {
			float: none; 
			border-top: 1px solid #6b727c;
			border-bottom: 1px solid #575f6a; position: relative;
		}
			nav ul ul li a {
				padding: 15px 40px;
				color: #fff;
			}	
				nav ul ul li a:hover {
					background: #4b545f;
				}
		
	nav ul ul ul {
		position: absolute; left: 100%; top:0;
	}
</style>
<nav>
	<ul>
		<li <?=($this->uri->segment(1)=="" ? 'class="active"' : '')?>><a href="<?='http://'.$_SERVER['SERVER_NAME']?>" <?=($this->uri->segment(1)=="" ? 'class="active"' : '')?>>Home</a></li>
		<li><a href="<?=site_url('home/berita')?>" <?=($this->uri->segment(2)=="berita" ? 'class="active"' : '')?>>Berita</a></li>
		<li <?=($this->uri->segment(2)=="graph" ? 'class="active"' : '')?>>
			<a href="<?=site_url('home/graph/r4/all/all/all')?>" <?=($this->uri->segment(2)=="graph" ? 'class="active"' : '')?>>Statistik Pengaduan</a>		</li>
		<li <?=($this->uri->segment(2)=="pengaduan" ? 'class="active"' : '')?>><a href="<?=site_url('home/pengaduan')?>" <?=($this->uri->segment(2)=="pengaduan" ? 'class="active"' : '')?>>Pengaduan Online</a>		</li>
	  <li <?=(($this->uri->segment(2)=="lihat_pengaduan" || $this->uri->segment(2)=="lihat_pengaduan_sms") ? 'class="active"' : '')?>><a href="#" <?=(($this->uri->segment(2)=="lihat_pengaduan" || $this->uri->segment(2)=="lihat_pengaduan_sms") ? 'class="active"' : '')?>>Lihat Pengaduan</a>
	    <ul>
	      <li><a href="<?=site_url('home/lihat_pengaduan')?>">Online</a></li>
          <li><a href="<?=site_url('home/lihat_pengaduan_sms')?>">SMS</a></li>
        </ul>
		</li>
        <li <?=($this->uri->segment(2)=="faq" ? 'class="active"' : '')?>><a href="<?=site_url('home/faq')?>" <?=($this->uri->segment(2)=="faq" ? 'class="active"' : '')?>>FAQ</a></li>
		<?php if($this->uri->segment(1)=='registrasi'):?>
		  <li <?=($this->uri->segment(1)=="registrasi" ? 'class="active"' : '')?>><a href="<?=site_url('registrasi')?>" <?=($this->uri->segment(1)=="registrasi" ? 'class="active"' : '')?>>Registrasi</a></li>
		  <?php endif;?>
	</ul>
</nav>