<head>
<title><?=$title?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="<?=$this->config->item('home_img')?>/favicon.png" />

<link href="<?=$this->config->item("home_plugin")?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$this->config->item("home_css")?>/style.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="<?=$this->config->item('home_js')?>/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('home_plugin')?>/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?=$this->config->item('home_js')?>/jquery.fusioncharts.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/cufon-yui.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/arial.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/cuf_run.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/piecemaker/web/scripts/swfobject/swfobject.js"></script>
 <script type="text/javascript">
      var flashvars = {};
      flashvars.cssSource = "<?=$this->config->item("home_plugin")?>/piecemaker/web/piecemaker.css";
      flashvars.xmlSource = "<?=site_url('home/slider')?>";
		
      var params = {};
      params.play = "true";
      params.menu = "false";
      params.scale = "showall";
      params.wmode = "transparent";
      params.allowfullscreen = "true";
      params.allowscriptaccess = "always";
      params.allownetworking = "all";
	  
      swfobject.embedSWF('<?=$this->config->item("home_plugin")?>/piecemaker/web/piecemaker.swf', 'piecemaker', '570', '300', '10', null, flashvars,    
      params, null);
    
    </script>

	<?php if($this->uri->segment(2)=="pengaduan" || $this->uri->segment(1)=="registrasi" || $this->uri->segment(2)=="" || $this->uri->segment(2)=="graph"|| $this->uri->segment(2)=="lihat_pengaduan" || $this->uri->segment(2)=="lihat_pengaduan_sms"):?>
	
<script type="text/javascript">
	$(document).ready(function() {
		$("#provinsi").change(function() {
			$("#kabkota").empty();
			$("#kabkota").append('<option value="<?=$this->uri->segment(2)=='' || $this->uri->segment(2)=='graph' ? 'all' : ''?>"><?=$this->uri->segment(2)=='' || $this->uri->segment(2)=='graph' || $this->uri->segment(2)=='lihat_pengaduan' || $this->uri->segment(2)=='lihat_pengaduan_sms'  ? '-Semua-' : '-Pilih-'?></option>');
			$("#kecamatan").empty();
			$("#kecamatan").append('<option value="">-Pilih-</option>');
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_kabkota')?>',
			{ KdProv : $("#provinsi option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#kabkota").append(
						'<option value="' + item.KdKab + '">'+item.NmKab + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#kabkota").change(function() {
			$("#kecamatan").empty();
			$("#kecamatan").append('<option value="">-Pilih-</option>');
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_kecamatan')?>',
			{ KdKab : $("#kabkota option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#kecamatan").append(
						'<option value="' + item.KdKec + '">'+item.NmKec + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#kecamatan").change(function() {
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_desa')?>',
			{ KdKec : $("#kecamatan option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#desa").append(
						'<option value="' + item.KdDesa + '">'+item.NmDesa + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#jenjang").change(function() {
			$("#namasek").empty();
			if($("#jenjang").val()==4){
				$("#namasek").append('lokasi');
			}else{
				$("#namasek").append('Nama Sekolah');
			}
		});
		
		/*function showketRP(){
			if($('input[name=KdKategori]:checked').val()==4){
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4").hide();
			}
		}
		showketRP();
		$('input[name=KdKategori]').click(function() {
			showketRP();
		});*/
		$("#KdKategori").change(function() {
			if($("#KdKategori").val()==4){
				$("#KetRpKat4_text").addClass("required");
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4_text").attr('class', 'textInput auto validateInteger');
				$("#KetRpKat4").attr('class', 'ctrlHolder');
				$("#KetRpKat4").hide();
			}
		});
		function showlainSumber(){
			if($('input[name=KdSumber]:checked').val()==8){
				$("#LainSumber").show();
			}else{
				$("#LainSumber").hide();
			}
		}
		showlainSumber();
		$('input[name=KdSumber]').click(function() {
			showlainSumber();
		});
		
				
		function showpLain(){
			if($('input[name=pLain]:checked').val()){
				$("#KetpLain").show();
			}else{
				$("#KetpLain").hide();
			}
		}
		showpLain();
		$('input[name=pLain]').click(function() {
			showpLain();
		});
	});
	
</script>

	<style>
	.side-a {
		float: left;
		width: 30%;
	}
	
	.side-c {
		float: left;
		width: 30%;
	}
	
	.side-b { 
		float: left;
		width: 30%;
	}
	</style>
	<link href="<?=$this->config->item('home_css')?>/uni-form.css" media="all" rel="stylesheet"/>
	<link href="<?=$this->config->item('home_css')?>/default.uni-form.css" media="all" rel="stylesheet"/>
	
	<?php endif;?>
	<?php if($this->uri->segment(2)=='' || $this->uri->segment(2)=='index' || $this->uri->segment(2)=='graph'):?>
<script type='text/javascript'>
	$(document).ready(function () {
		Startup();
	});
	
function Startup(){
	// load the chart
	view_chart();
	
}

</script>


<?=$graph?>
<?php endif;?>
<!--jquery ui-->
<link rel="stylesheet" href="<?=$this->config->item("admin_js")?>/jquery-ui/themes/redmond/jquery.ui.all.css">
<script src="<?=$this->config->item("admin_js")?>/jquery-ui/ui/jquery-ui-1.8.18.custom.js"></script>
<!--end jquery ui-->
<!--dialog-->
<script>
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#dialog-registrasi" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "blind",
			
			width: '500px'
		});

		$( "#registrasi" ).click(function() {
			$( "#dialog-registrasi" ).dialog( "open" );
			return false;
		});
		$( "#dialog-forget" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "blind",
			
			width: '500px'
		});

		$( "#forget" ).click(function() {
			$( "#dialog-forget" ).dialog( "open" );
			return false;
		});
	});
	</script>
<!--end dialog-->
</head>