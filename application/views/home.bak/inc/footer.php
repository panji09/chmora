<footer>
    <div class="footer_content">
	
		<div class="fot_1">
			<img src="<?=$this->config->item('home_img')?>/logo-disdik-dki.png" />
		</div>
		<div class="fot_1">
			<h1>Link Pendukung</h1>
			
			<p>
				Pemerintah Provinsi DKI Jakarta - <a href="http://www.jakarta.go.id/">www.jakarta.go.id</a>
			</p>
			<p>
				Dinas Pendidikan Pemerintah Provinsi DKI Jakarta - <a href="http://disdik.jakarta.go.id/">disdik.jakarta.go.id</a>
			</p>
			<p>
				Kementerian Pendidikan dan Kebudayaan Republik Indonesia - <a href="http://www.kemdikbud.go.id">www.kemdikbud.go.id</a>
			</p>
			
			<p>
				Bantuan Operasional Sekolah - <a href="http://bos.kemdikbud.go.id">bos.kemdikbud.go.id</a>
			</p>
			<p>
				<a href="#"><img src="<?=$this->config->item('home_img')?>/twitter.png" /></a>
				<a href="#"><img src="<?=$this->config->item('home_img')?>/facebook.png" /></a>
			</p>
		</div>
		<div class="fot_1">
			<h1>Hubungi Kami</h1>
			<p>
				Jalan Gatot Subroto Kavling 40-41, Jakarta Selatan,<br />
				 DKI Jakarta - Indonesia
			</p>
			<p>
		  <ul>
			  <li>Alamat Web:  <a href="http://bsm.kemdikbud.go.id">http://disdik.jakarta.go.id/</a></li>
			  <li>Nomor Telephone:  # </li>
			  <li>Faksimili: #</li>
			  <li>Email: <a href="mailto:#">#</a></li>
				</ul>
			
			</p>
		</div>
		
		<div class="fot_2">
			Copyright &copy; <?=(date('Y')==2014 ? date('Y') : '2014 - '.date('Y'))?> Dinas Pendidikan Pemerintah Provinsi DKI Jakarta. All Right Reserved
		</div>
	
	</div>
</footer>