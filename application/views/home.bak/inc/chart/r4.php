<?php 
// kategori & status
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_kategori" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var pending = [219,40,97,48];
	var proses = [219,40,97,48];
	var selesai = [1560,201,496,88];
	var ticks = ['Pertanyaan','Saran','Pengaduan'];
	 
	plot2 = $.jqplot('chart_kategori', [pending, proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
			pointLabels: { show: true }
		},
		axes: {
			xaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks
			}
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Pending'},
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENGADUAN BERDASARKAN KATEGORI <br>& STATUS PENANGANAN 2014'
	   }
	});
 
	
});
</script>
			<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (pending / proses / selesai)</p>
			<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			  <tr>
				  <th>Kategori</th>
				  <th>Pending</th>
				  <th>Proses</th>
				  <th>Selesai</th>
				  <th>Total</th>
			  </tr>

			  <tr>
				  <td>Pertanyaan</td>
				  <td>219</td>
				  <td>219</td>
				  <td>1560</td>
				  <td>1779</td>
				  </tr>
			  <tr>
				  <td>Saran</td>
				  <td>40</td>
				  <td>40</td>
				  <td>201</td>
				  <td>241</td>
				  </tr>
			  <tr>
				  <td>Pengaduan</td>
				  <td>97</td>
				  <td>97</td>
				  <td>496</td>
				  <td>593</td>
				  </tr>
			  
			  <tr>
				  <td>Total</td>
				  <td>404</td>
				  <td>404</td>
				  <td>2345</td>
				  <td>2749</td>
			  </tr>
			
			
			</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>