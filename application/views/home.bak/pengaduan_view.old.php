<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="#"><span>Layanan Masyarakat & </span>Penanganan Pengaduan</a> <small><!--Simple design templates--></small></h1>
      </div>
      <div class="clr"></div>
      <?php require_once("inc/menu.php"); ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="mainbar" style="width:inherit">
        <div class="article">
          <h2>Pengaduan Masyarakat </h2>
          <div class="clr"></div>
          <p class="infopost"><!--Posted <span class="date">on 11 sep 2018</span> by <a href="#">Owner</a> &nbsp;|&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> with <a href="#" class="com">Comments <span>11</span></a>--></p>
			<?php if($this->uri->segment(2)=="pengaduan"):?>
			<form action="<?=site_url('home/post_pengaduan')?>" method="post">
			<fieldset>
		<div class="side-a">
			<fieldset>
			<legend>Lokasi Kejadian</legend>
			<dl>
				<dt>Provinsi</dt>
				<dd>
					<select name="KdProv" id="provinsi">
						<option>-Pilih-</option>
						<?php foreach($prov->result() as $row):?>
						<option value="<?=$row->KdProv?>"><?=$row->NmProv?></option>
						<?php endforeach;?>
					</select>
				</dd>
				<dt>Kabupaten / Kota</dt>
				<dd>
					<select id="kabkota" name="KdKab">
						<option>-Pilih-</option>
					</select>
				</dd>
				<dt>Kecamatan</dt>
				<dd>
					<select id="kecamatan" name="KdKec">
						<option>-Pilih-</option>
					</select>
				</dd>
				<dt>Desa</dt>
				<dd>
					<select name="KdDesa" id="desa">
						<option>-Pilih-</option>
					</select>
				</dd>
				<dt>Jenjang</dt>
				<dd>
					<select name="KdJenjang" id="jenjang">
						<option value="">-Pilih-</option>
						<option value="1">SD</option>
						<option value="2">SMP</option>
						<option value="3">SD & SMP</option>
						<option value="4">Lainnya</option>
					</select>
				</dd>
				
				<dt id="namasek"></dt>
				<dd>
					<textarea name="NmSekolah"></textarea>
				</dd>
			</dl>
			
			</fieldset>
			<fieldset>
			<legend>Kategori Pengaduan</legend>
			<dl>
				<dt><input class="KdKategori" type="radio" name="KdKategori" value="1"/>Pertanyaan</dt>
				<dt><input class="KdKategori" type="radio" name="KdKategori" value="2"/>Saran</dt>
				<dt><input class="KdKategori" type="radio" name="KdKategori" value="3"/>Penyimpangan Peraturan</dt>
				<dt><input class="KdKategori" type="radio" name="KdKategori" value="4"/>Ketidaksesuaian Penggunaan Dana</dt>
				<dd id="KetRpKat4">Rp. <input type="text" name="KetRpKat4"/></dd>
			</dl>
			</fieldset>
		</div>
		<div class="side-b">
			<fieldset>
			<legend>Identitas Pelapor </legend>
			<dl>
				<dt>Profesi</dt>
				<dd>
					<select>
						<option></option>
					</select>
				</dd>
				<dt>Nama</dt>
				<dd>
					<input type="text" />
				</dd>
				<dt>Telp/Email</dt>
				<dd><input type="text" /></dd>
				<dt>Alamat</dt>
				<dd><textarea></textarea></dd>
				<dt><input type="radio" name="KdSumber" value="1" class="KdSumber"/>Orangtua Siswa</dt>
				<dt><input type="radio" name="KdSumber" value="2" class="KdSumber"/>Komite Sekolah</dt>
				<dt><input type="radio" name="KdSumber" value="3" class="KdSumber"/>Guru / Pegawai Sekolah</dt>
				<dt><input type="radio" name="KdSumber" value="4" class="KdSumber"/>Kepala Sekolah</dt>
				<dt><input type="radio" name="KdSumber" value="5" class="KdSumber"/>Tim Management BOS</dt>
				<dt><input type="radio" name="KdSumber" value="6" class="KdSumber"/>Media Masa</dt>
				<dt><input type="radio" name="KdSumber" value="7" class="KdSumber"/>Hasil Audit</dt>
				<dt><input type="radio" name="KdSumber" value="8" class="KdSumber"/>Lain - lain</dt>
				<dd id="LainSumber"><input type="text" name="LainSumber"/></dd>
			</dl>
			</fieldset>
			<fieldset>
			<legend>Pengaduan</legend>
			<dl>
				<dt>Deskripsi Pengaduan</dt>
				<dd><textarea name="Deskripsi"></textarea></dd>
			</dl>
			</fieldset>
		</div>
		<div class="side-c">
			<fieldset>
			<legend>Sumber Informasi</legend>
			<dl>
				<dt><input type="checkbox" name="pKomite"/>Komite Sekolah</dt>
				<dt><input type="checkbox" name="pGuru"/>Guru / Pegawai Sekolah</dt>
				<dt><input type="checkbox" name="pBendSek"/>Kepala / Bendahara Sekolah</dt>
				<dt><input type="checkbox" name="pLSM"/>LSM</dt>
				<dt><input type="checkbox" name="pDinas"/>Dinas Pendidikan</dt>
				<dt><input type="checkbox" name="pUPTD"/>UPTD / Pengawas</dt>
				<dt><input type="checkbox" name="pLain" id="pLain"/>Lain - lain</dt>
				<dd id="KetpLain"><input type="text" name="KetpLain"/></dd>
			</dl>
			<dl><input type="submit"  value="Submit"/></dl>
			</fieldset>
		</div>
	</fieldset>
</form>
			<?php elseif($this->uri->segment(2)=="faq"):?>
				<?=$faq->isi?>
			<?php endif;?>
          <p>&nbsp;</p>
        </div>
        <p class="pages"><!--<small>Page 1 of 2 &nbsp;&nbsp;&nbsp;</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a>--></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
  </div>
</div>
</body>
</html>
