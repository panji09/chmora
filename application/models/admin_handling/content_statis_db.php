<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_statis_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function exist($id){
        return $this->db->get_where('content_statis',  array('id' => $id));
    }
    function save($id='about', $data_content, $data_content_statis=array('published' => 1, 'deleted'=> 0)){
        $result=false;
        
        $this->db->trans_start();
        
        $exist=$this->exist($id);
        if($exist->num_rows() == 1){
            //update
            $content_statis=$exist->row();
            $this->db->where('id', $content_statis->content_id);
            if($this->db->update('content',$data_content)){
                $this->db->where('id', $id);
                $result=$this->db->update('content_statis',$data_content_statis);
            }
            
            
            
        }else{
            //insert
            if($this->db->insert('content',$data_content)){
                $data_content_statis['content_id']=$this->db->insert_id();
                $result = $this->db->insert('content_statis',$data_content_statis);
            }
        }
        $this->db->trans_complete();
        
        return $result;
    }
    
    function get($id){
        $this->db->select('content_statis.id, content.title, content.content, content.last_update, person.nama, content_statis.published');
        $this->db->from('content_statis');
        $this->db->join('content', 'content.id = content_statis.content_id','inner');
        $this->db->join('person', 'content.author = person.id','inner');
        
        $this->db->where('content_statis.id',$id);
        
        $this->db->where('content_statis.deleted', 0);
        
        $query=$this->db->get();
        
        if($query->num_rows() == 1){
            return $query->row();
        }else{
            $content_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $content_obj->$field = '';
            }
            return $content_obj;
        }
    }
}
?>