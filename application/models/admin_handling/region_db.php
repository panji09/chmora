<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Region_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
    }
    
    function provinsi($data=null){
        if(isset($data['id']))
            $this->db->where('id',$data['id']);
        $this->db->order_by('name', 'ASC');
        return $this->db->get('provinsi');
    }
    function kabkota($data=null){
        if(isset($data['id']))
            $this->db->where('id',$data['id']);
        else
            $this->db->where('provinsi_id',$data['provinsi_id']);
        $this->db->order_by('name','ASC');
        return $this->db->get('kabkota');
    }
    function kecamatan($data=null){
        if(isset($data['id']))
            $this->db->where('id',$data['id']);
        else
            $this->db->where('kabkota_id',$data['kabkota_id']);

        $this->db->order_by('name','ASC');
        return $this->db->get('kecamatan');
    }
}
?>